class Game
  attr_accessor :comp_board, :user_board

  def initialize
    @user_board = Board.new("user")
    @comp_board = Board.new("comp")
    @comp_board.put_bombs
    @comp_board.process_rows
    display_board
  end

  def run
    loop do
      puts "What's your move?"
      input  = String(gets.chomp)
      action = input.scan(/[a-zA-Z]/).first.to_s
      action = "r" if action == ""
      row_i  = input.scan(/\d(?=\,)/).first.to_i
      cell_i = input.scan(/(?<=\,)\d/).first.to_i

      output = @user_board.user_move(action,row_i,cell_i,@comp_board)
      display_board

      if output == "Game Over"
        puts "Game Over"
        break
      end

      left = 0
      @user_board.rows.each do |row|
        left += row.count("*")
      end
      if @user_board.num_bombs == left
        puts "You Won!!!"
        break
      end

    end
  end

  def display_board
    # @comp_board.display
    # puts
    @user_board.display
    puts
  end

end

class Board
  attr_accessor :rows, :num_bombs

  def initialize(type)
    type == "user" ? @type = "*" : @type = 0
    @num_bombs  = 1
    @rows       = create_board
    @used_cells = []
  end

  def create_board
    board = []

    9.times do
      row = Array.new
      9.times do
        row << @type
      end
      board << row
    end

    board
  end

  def put_bombs
    @num_bombs.times do
      put_bomb
    end
  end

  def put_bomb
    cell_i  = rand(9)
    row_i = rand(9)

    @rows[row_i][cell_i] == "b" ? put_bomb : @rows[row_i][cell_i] = "b"
  end

  def process_rows
    @rows.each_with_index do |row,row_i|
      row.each_with_index do |cell,cell_i|
        process_cell(row_i,cell_i)
      end
    end
  end

  def process_cell(cell_i,row_i)
    unless @rows[row_i][cell_i] == "b"

      if row_i > 0 && cell_i > 0 && @rows[row_i-1][cell_i-1] == "b"
        @rows[row_i][cell_i] += 1
      end

      if cell_i > 0 && @rows[row_i][cell_i-1] == "b"
        @rows[row_i][cell_i] += 1
      end

      if row_i < 8 && cell_i > 0 && @rows[row_i+1][cell_i-1] == "b"
        @rows[row_i][cell_i] += 1
      end

      if row_i > 0 && @rows[row_i-1][cell_i] == "b"
        @rows[row_i][cell_i] += 1
      end

      if row_i < 8 && @rows[row_i+1][cell_i] == "b"
        @rows[row_i][cell_i] += 1
      end

      if row_i > 0 && cell_i < 8 && @rows[row_i-1][cell_i+1] == "b"
        @rows[row_i][cell_i] += 1
      end

      if cell_i < 8 && @rows[row_i][cell_i+1] == "b"
        @rows[row_i][cell_i] += 1
      end

      if row_i < 8 && cell_i < 8 && @rows[row_i+1][cell_i+1] == "b"
        @rows[row_i][cell_i] += 1
      end

    end
  end

  def display
    puts
    @rows.each do |row|
      if @type == "*"
        puts row.map{|el| el == 0 ? "_" : el }.join(' ')
      else
        puts row.join(' ')
      end
    end
  end

  def user_move(action="r",row_i,cell_i,comp_board)
    puts "Action: #{action} | Row: #{row_i} | Cell: #{cell_i}"

    @rows[row_i][cell_i] = action if action == "f"

    if action == "r"
      comp = comp_board.rows[row_i][cell_i]
      @rows[row_i][cell_i] = comp
      return "Game Over" if comp == "b"
      process_empty_case(row_i,cell_i,comp_board) if comp == 0
    end
  end

  def process_empty_case(row_i,cell_i,comp_board)

    if row_i > 0 && cell_i > 0
      x = cell_i-1
      y = row_i-1
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

    if cell_i > 0
      x = cell_i-1
      y = row_i
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

    if row_i < 8 && cell_i > 0
      x = cell_i-1
      y = row_i+1
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

    if row_i > 0
      x = cell_i
      y = row_i-1
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

    if row_i < 8
      x = cell_i
      y = row_i+1
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

    if row_i > 0 && cell_i < 8
      x = cell_i+1
      y = row_i-1
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

    if cell_i < 8
      x = cell_i+1
      y = row_i
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

    if row_i < 8 && cell_i < 8
      x = cell_i+1
      y = row_i+1
      if comp_board.rows[y][x] == 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
          process_empty_case(y,x,comp_board)
        end
      elsif comp_board.rows[y][x] > 0
        unless @used_cells.include?([y,x])
          @rows[y][x] = comp_board.rows[y][x]
          @used_cells << [y,x]
        end
      end
    end

  end

end

g = Game.new
g.run
